<?php
require_once __DIR__ . '/vendor/autoload.php';

$mysqli = new mysqli("localhost", "root", "", "report");
if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = 0;
}

$sql = "SELECT a.report_kegiatan AS nama,
               b.jenis_nama AS jenis,
               a.report_unit AS unit,
               a.report_dasar AS dasar,
               DATE_FORMAT(a.report_tanggal, '%W, %e %M %Y') AS tanggal,
               a.report_tempat AS tempat,
               a.report_pimpinan_nama AS pimpinan,
               a.report_pimpinan_jabatan AS jabatan,
               a.report_peserta AS peserta,
               a.report_hasil AS hasil,
               a.report_img1 AS img1,
               a.report_img2 AS img2,
               a.report_img3 AS img3,
               a.report_img4 AS img4,
               a.report_img5 AS img5,
               c.user_nama AS notulis
        FROM tb_report a
        JOIN tb_jenis_kegiatan b ON b.jenis_id = a.report_jenis
        JOIN tb_user c ON c.user_id = a.report_publisher
        WHERE a.report_id = " . $id;
$result = $mysqli->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $html = "<html><head><style>
                table {
                    font-family: Arial;
                    border-collapse: collapse;
                    width: 100%;
                }
            
                table thead tr td {
                    padding: 16px;
                }
            
                table tbody tr td {
                    padding: 8px;
                }
            
                td {
                    border: 1px solid #000000;
                    text-align: left;
                }
            
                .kementerian {
                    font-size: 18px;
                    font-weight: bold;
                }
            
                .balai {
                    font-size: 12px;
                    font-weight: bold;
                }
            
                .empty {
                    height: 32px;
                }
            </style>
            </head><body><table>
                <tbody>
                <tr>
                    <td width=\"25%\" style=\"align-content: center\"><img src=\"logo.jpg\" width=\"140px\"></td>
                    <td><span class=\"kementerian\">KEMENTERIAN PERHUBUNGAN<br>
                        DIREKTORAT JENDERAL PERKERETAAPIAN<br></span>
                        <span class=\"balai\">BALAI TEKNIK PERKERETAAPIAN WILAYAH SUMATERA BAGIAN UTARA<br></span>
                        Jalan Timor No.29 Medan<br>
                        Telp.(061) 88814922, Fax.(061) 88813611<br>
                        Email: transkasumut@katscenter.com
                    </td>
                </tr>                               
                <tr class=\"empty\">
                    <td colspan=\"2\"></td>
                </tr>
                <tr>
                    <td>Nama Kegiatan</td>
                    <td>" . $row["nama"] . "</td>
                </tr>
                <tr>
                    <td>Jenis Kegiatan</td>
                    <td>" . $row["jenis"] . "</td>
                </tr>
                <tr>
                    <td>Unit Kerja Terkait</td>
                    <td>" . $row["unit"] . "</td>
                </tr>
                <tr>
                    <td>Dasar Pelaksanaan</td>
                    <td>" . $row["dasar"] . "</td>
                </tr>
                <tr>
                    <td>Tanggal &amp; Waktu Pelaksanaan</td>
                    <td>" . $row["tanggal"] . "</td>
                </tr>
                <tr>
                    <td>Tempat</td>
                    <td>" . $row["tempat"] . "</td>
                </tr>
                <tr>
                    <td>Pimpinan Kegiatan</td>
                    <td>" . $row["pimpinan"] . "<br>" . $row["jabatan"] . "</td>
                </tr>
                <tr>
                    <td>Peserta</td>
                    <td>" . $row["peserta"] . "</td>
                </tr>
                <tr class=\"empty\">
                    <td colspan=\"2\"></td>
                </tr>
                <tr>
                    <td>Hasil</td>
                    <td>" . $row["hasil"] . "<br><img src='" . $row["img1"] . "'><br><img src='" . $row["img2"] . "'><br><img src='" . $row["img3"] . "'><br><img src='" . $row["img4"] . "'><br><img src='" . $row["img5"] . "'></td>
                </tr>
                <tr>
                    <td>Yang Melaporkan</td>
                    <td>" . $row["notulis"] . "</td>
                </tr>
                </tbody>
            </table>
            </body></html>";

        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        echo "report_kegiatan: " . $row["report_kegiatan"] . " - report_dasar: " . $row["report_dasar"] . "<br>";
    }
} else {
    echo "0 results";
}
$mysqli->close();
?>