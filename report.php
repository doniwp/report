<style>
    table {
        font-family: Arial;
        border-collapse: collapse;
        width: 100%;
    }

    table thead tr td {
        padding: 16px;
    }

    table tbody tr td {
        padding: 8px;
    }

    td {
        border: 1px solid #000000;
        text-align: left;
    }

    .kementerian {
        font-size: 24px;
        font-weight: bold;
    }

    .balai {
        font-size: 18px;
        font-weight: bold;
    }

    .empty {
        height: 32px;
    }
</style>
<table>
    <thead>
    <tr>
        <td width="10%" style="align-content: center"><img src="logo.jpg" width="180px"></td>
        <td><span class="kementerian">KEMENTERIAN PERHUBUNGAN<br>
            DIREKTORAT JENDERAL PERKERETAAPIAN<br></span>
            <span class="balai">BALAI TEKNIK PERKERETAAPIAN WILAYAH SUMATERA BAGIAN UTARA<br></span>
            Jalan Timor No.29 Medan<br>
            Telp.(061) 88814922, Fax.(061) 88813611<br>
            Email: transkasumut@katscenter.com
        </td>
    </tr>
    </thead>
    <tbody>
    <tr class="empty">
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>Nama Kegiatan</td>
        <td></td>
    </tr>
    <tr>
        <td>Jenis Kegiatan</td>
        <td></td>
    </tr>
    <tr>
        <td>Unit Kerja Terkait</td>
        <td></td>
    </tr>
    <tr>
        <td>Dasar Pelaksanaan</td>
        <td></td>
    </tr>
    <tr>
        <td>Tanggal & Waktu Pelaksanaan</td>
        <td></td>
    </tr>
    <tr>
        <td>Tempat</td>
        <td></td>
    </tr>
    <tr>
        <td>Pimpinan Kegiatan</td>
        <td></td>
    </tr>
    <tr>
        <td>Peserta</td>
        <td></td>
    </tr>
    <tr class="empty">
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>Hasil</td>
        <td></td>
    </tr>
    <tr>
        <td>Yang Melaporkan</td>
        <td></td>
    </tr>
    </tbody>
</table>
